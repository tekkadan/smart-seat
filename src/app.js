'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const _ = require('lodash');

const Scenario = require('./models/Scenario');
const ScenarioType = require('./constants/ScenarioType');
const Seat = require('./models/Seat');
const SeatManager = require('./models/SeatManager');
const seatManagerService = require('./services/seat-manager');
const simulatorService = require('./services/simulator');


var app = express();

app.use(cors())
  .use(bodyParser.urlencoded({extended: true}))
  .use(bodyParser.json({limit: '5mb', type:'application/json'}));
  
var seatAmount = 20;
var seats = _.map(_.range(0,seatAmount), function(i){ return new Seat(i) });
var seatManager = new SeatManager({seats: seats});
seatManager.addScenario(new Scenario('default', ScenarioType.Type.Normal, { default: true }));

app.use('/', seatManagerService(seatManager))
  .use('/simulator', simulatorService(seatManager));


module.exports = app;