'use strict';

var Status = function(code, message){
	this.code = code;
	this.message = message;
}

Status.prototype.toString = function(seat){
	return JSON.stringify(this);
}

module.exports = Status;