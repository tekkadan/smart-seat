'use strict';

const _ = require('lodash');
const Group = require('./Group');
const Status = require('./Status');
const ScenarioType = require('../constants/ScenarioType');
const shuffleArray = require('../helpers/shuffleArray');
const randomColor = require('../helpers/randomColor');

var SeatManager = function(options){
	this.seats = options.seats || [];
	this.scenarios = [];
	this.startScheduler();
}

SeatManager.prototype.startScheduler = function(){
	var self = this;

	this.scheduler = setInterval(function(){
		self.updateCurrentScenario();
	},1000);
}

SeatManager.prototype.updateCurrentScenario = function(){
	var scenario = this.getCurrentScenario();

	if(scenario == this.scenario) return;

	this.setOverallScenario(scenario);
}

SeatManager.prototype.setOverallScenario = function(scenario){
	this.scenario = scenario;

	_.forEach(this.seats, function(seat){
		seat.setScenario(scenario);
	});

	this.resetSeats();

	return this.scenario;
}

SeatManager.prototype.assignRandomGroup = function(n){
	if(this.scenario.type != ScenarioType.Type.Class)
		return new Status(403, 'Unable to assign random group on '+this.scenario.type+' scenario');

	var occupiedSeats = this.getOccupiedSeats();
	var shuffledSeats = shuffleArray(occupiedSeats);
	var groups = [];

	for(var i = 0; i < n; i++){
		groups.push(new Group(i, randomColor()));
	}

	for(var i = 0; i < shuffledSeats.length; i++){
		var group = groups[i % n];
		var seat = shuffledSeats[i];

		group.addSeat(seat);
		seat.setGroup(group);
	}

	return groups;
}

SeatManager.prototype.setOccupant = function(seat, person, duration){
	if(!seat.isAvailable())
		return new Status(403, 'Seat is unavailable');

	if(this.scenario.type == ScenarioType.Type.Class){
		var self = this;
		var previousSeats = _.map(_.range(0,seat.id), function(i){ return self.getSeatById(i); });
		var occupiable = _.every(previousSeats, function(prevSeat){ return !prevSeat.isAvailable(); });

		if(occupiable)
			return seat.setOccupant(person, duration);
		return new Status(403, 'Please occupy the first unoccupied seat from the front');
	}

	if(this.scenario.type == ScenarioType.Type.Test){
		if(seat.getPredefinedOccupant() && seat.getPredefinedOccupant().id == person.id)
			return seat.setOccupant(person, duration);
		return new Status(403, 'This is not your seat');
	}

	if(this.scenario.type == ScenarioType.Type.Normal){
		return seat.setOccupant(person, duration);
	}

	return new Status(404, 'Unknown scenario');
}

SeatManager.prototype.clearOccupant = function(seat){
	return seat.clearOccupant();
}

SeatManager.prototype.clearOccupants = function(){
	var self = this;

	return _.forEach(this.seats, function(seat){
		self.clearOccupant(seat);
	});
}

SeatManager.prototype.clearPredefinedOccupant = function(seat){
	return seat.clearPredefinedOccupant();
}

SeatManager.prototype.clearPredefinedOccupants = function(){
	var self = this;

	return _.forEach(this.seats, function(seat){
		self.clearPredefinedOccupant(seat);
	});
}

SeatManager.prototype.clearGroup = function(seat){
	return seat.clearGroup();
}

SeatManager.prototype.clearGroups = function(){
	var self = this;

	return _.forEach(this.seats, function(seat){
		self.clearGroup(seat);
	});
}

SeatManager.prototype.resetSeats = function(){
	return this.clearOccupants() && this.clearPredefinedOccupants() && this.clearGroups();
}

SeatManager.prototype.setPredefinedOccupant = function(seat, occupant){
	if(this.scenario.type != ScenarioType.Type.Test)
		return new Status(403, 'Unable to set predefined occupant on '+this.scenario.type+' scenario');

	return seat.setPredefinedOccupant(occupant);
}

SeatManager.prototype.getOverallScenario = function(){
	return this.scenario;
}

SeatManager.prototype.getAvailableSeats = function(){
	return _.filter(this.seats, function(seat){
		return seat.isAvailable();
	});
}

SeatManager.prototype.getOccupiedSeats = function(){
	return _.filter(this.seats, function(seat){
		return seat.getOccupant();
	});
}

SeatManager.prototype.getOccupant = function(seat){
	return seat.getOccupant();
}

SeatManager.prototype.getSeatById = function(id){
	return _.find(this.seats, { id: id });
}

SeatManager.prototype.getCurrentScenario = function(){
	return _.maxBy(_.filter(this.scenarios, { active: true }), 'priority');
}

SeatManager.prototype.getScenarios = function(){
	return this.scenarios;
}

SeatManager.prototype.addScenario = function(scenario){
	scenario.start();
	return this.scenarios.push(scenario);
}

SeatManager.prototype.removeScenario = function(scenario){
	scenario.end();
	return _.remove(this.scenarios, { id: scenario.id });
}

SeatManager.prototype.getScenarioById = function(id){
	return _.find(this.scenarios, { id: id });
}

module.exports = SeatManager;
