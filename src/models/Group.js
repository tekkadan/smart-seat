'use strict';

var Group = function(id, color){
	this.id = id;
	this.color = color;
	this.seats = [];
}

Group.prototype.addSeat = function(seat){
	this.seats.push(seat);

}

Group.prototype.clearSeats = function(seat){
	this.seats = [];
}

module.exports = Group;