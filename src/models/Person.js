'use strict';

var Person = function(id, name){
	this.id = id;
	this.name = name;
}

module.exports = Person;