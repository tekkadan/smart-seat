'use strict';

const moment = require('moment');

var Scenario = function(id, type, options){
	options = options || {};

	this.id = id;
	this.type = type;
	this.priority = options.priority || 0;
	this.default = options.default || false;

	if(this.default){
		this.active = true;
		return;
	}

	if(!options.startedAt || !options.endedAt)
		throw new Error('Please specify startedAt and endedAt');

	this.active = false;
	this.startedAt = options.startedAt;
	this.endedAt = options.endedAt;
}

Scenario.prototype.start = function(){
	if(this.default) return;

	var self = this;
	this.scheduler = setInterval(function(){ self.update(); }, 1000);
}

Scenario.prototype.end = function(){
	if(this.default) return;

	clearInterval(this.scheduler);
}

Scenario.prototype.update = function(){
	var currentTime = moment();
	var startedTime = moment(this.startedAt, 'HH:mm:ss');
	var endedTime 	= moment(this.endedAt, 'HH:mm:ss');

	if(startedTime < currentTime && currentTime < endedTime){
		this.active = true;
		return;
	}
	this.active = false;
}

module.exports = Scenario;