'use strict';

const Status = require('./Status');

var Seat = function(id){
	this.id = id;
	this.available = true;
	this.occupant = null;
	this.predefinedOccupant = "";
	this.expirationTime = 0;
}

Seat.prototype.setOccupant = function(occupant, duration){
	this.clearOccupant();
	this.occupant = occupant;
	this.expirationTime = Math.floor(new Date() / 1000) + duration;
	
	var self = this;
	this.expirer = setTimeout(function(){
		self.clearOccupant();
	}, duration * 1000);

	return this;
}

Seat.prototype.setPredefinedOccupant = function(predefinedOccupant){
	this.predefinedOccupant = predefinedOccupant;
	return this;
}

Seat.prototype.setScenario = function(scenario){
	this.scenario = scenario;
	return this;
}

Seat.prototype.setAvailability = function(available){
	this.available = available;
	return this;
}

Seat.prototype.setSeatNumber = function(number){
	this.number = number;
	return this;
}

Seat.prototype.setGroup = function(group){
	this.group = group;
	return this;
}

Seat.prototype.clearGroup = function(){
	this.group = null;
	return this;
}

Seat.prototype.clearOccupant = function(){
	this.occupant = null;
	this.expirationTime = 0;
	clearTimeout(this.expirer);
	return this;
}

Seat.prototype.clearPredefinedOccupant = function(){
	this.predefinedOccupant = null;
	return this;
}

Seat.prototype.getOccupant = function(){
	if(this.occupant && this.expirationTime < (new Date() / 1000))
		this.clearOccupant();

	return this.occupant;
}

Seat.prototype.getPredefinedOccupant = function(){
	return this.predefinedOccupant;
}

Seat.prototype.getScenario = function(){
	return this.scenario;
}

Seat.prototype.isAvailable = function(){
	return this.available && !this.getOccupant();
}

Seat.prototype.getSeatNumber = function(){
	return this.number;
}

Seat.prototype.getGroup = function(){
	return this.group;
}

module.exports = Seat;
