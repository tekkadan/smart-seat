'use strict';

const express = require('express');
const _ = require('lodash');
const Person = require('../../models/Person');
const Status = require('../../models/Status');
const Scenario = require('../../models/Scenario');
const ScenarioType = require('../../constants/ScenarioType');
const CircularJSON = require('circular-json');

module.exports = function(seatManager){
	var app = express();

	app.get('/index.php', function(req, res){
		var func 		= req.query.fungsi;
		var id_device 	= parseInt(req.query.id_device);
		var jml_arg 	= parseInt(req.query.jml_arg) || 0;

		var seat = seatManager.getSeatById(id_device);
		var result;

		if(func == 'setOverallScenario'){
			result = seatManager.setOverallScenario(
				ScenarioType.fromString(req.query.arg1)
			);
		}

		if(func == 'assignRandomGroup'){
			result = seatManager.assignRandomGroup(parseInt(req.query.arg1));
		}

		if(func == 'setOccupant'){
			result = seatManager.setOccupant(
				seat,
				new Person(req.query.arg1, req.query.arg2),
				parseInt(req.query.arg3)
			);
		}

		if(func == 'setPredefinedOccupant'){
			result = seatManager.setPredefinedOccupant(
				seat,
				new Person(req.query.arg1, req.query.arg2)
			);
		}

		if(func == 'addScenario'){
			result = seatManager.addScenario(new Scenario(req.query.arg1, ScenarioType.fromString(req.query.arg2), { 
				priority: parseInt(req.query.arg3),
				startedAt: req.query.arg4,
				endedAt: req.query.arg5,
			}));
		}

		if(func == 'removeScenario'){
			result = seatManager.removeScenario(seatManager.getScenarioById(req.query.arg1));
		}

		if(func == 'getOccupant'){
			result = seatManager.getOccupant(seat);
		} else if((_.includes(func, 'get') || _.includes(func, 'clear')) && typeof seatManager[func] === "function"){
			result = seatManager[func]();
		}

		res.setHeader('Content-Type', 'application/json');

		if(result === undefined || result == null){
			res.send(CircularJSON.stringify({ status: false, message: "Undefined" }));
			return;
		}

		res.send(CircularJSON.stringify({ status: true, data: result }));
	});

	return app;
};
