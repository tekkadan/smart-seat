'use strict';

const express = require('express');
const CircularJSON = require('circular-json');
const _ = require('lodash');
const Person = require('../../models/Person');

module.exports = function(seatManager){
	var app = express();

	app.set('view engine', 'pug');
	app.set('views', __dirname + '/views');

	app.use('/assets', express.static(__dirname + '/assets'));

	app.locals.app = app;

	app.get('/', function(req, res){
		res.render('pages/home', {seatManager: seatManager});
	});

	app.post('/occupy', function(req, res){
		var seatID = req.body.seat_id;
		var personID = req.body.person_id;
		var personName = req.body.person_name;
		var duration = req.body.duration;

		var person = new Person(personID, personName);
		var seat = _.find(seatManager.seats, { 'id': seatID });

		seatManager.setOccupant(seat, person, duration);

		res.json({status: 'success'});
	});

	app.post('/unoccupy', function(req, res){
		var seatID = req.body.seat_id;
		var seat = _.find(seatManager.seats, { 'id': seatID });

		seatManager.clearOccupant(seat);
		
		res.json({status: 'success'});
	});

	return app;
}