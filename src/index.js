'use strict';

const ip = require('ip');
const app = require('./app');
const ssdp = require('./ssdp');
const port = process.env.PORT || 9000;
const server = app.listen(port);

ssdp.server({ location: ip.address()+':'+port });
ssdp.client();

server.on('listening', () =>
  console.log(`Application started on 127.0.0.1:${port}`)
);