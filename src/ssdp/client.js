var SSDP = require('node-ssdp');

module.exports = function(){
	var client = new SSDP.Client();

	client.on('notify', function () {
		//console.log('Got a notification.')
	})

	client.on('response', function inResponse(headers, code, rinfo) {
		console.log('Got a response to an m-search:\n%d\n%s\n%s', code, JSON.stringify(headers, null, '  '), JSON.stringify(rinfo, null, '  '))
	})

	return client;	
}