var SSDP = require('node-ssdp');

module.exports = function(options){
	var server = new SSDP.Server({
		location: options.location || require('ip').address()
	});

	server.addUSN('upnp:rootdevice');
	server.addUSN('urn:schemas-upnp-org:device:SeatManager:1');

	server.on('advertise-alive', function (heads) {
		// console.log('advertise-alive', heads)
		// Expire old devices from your cache.
		// Register advertising device somewhere (as designated in http headers heads)
	});

	server.on('advertise-bye', function (heads) {
		// console.log('advertise-bye', heads)
		// Remove specified device from cache.
	});

	server.start('0.0.0.0');

	return server;
}