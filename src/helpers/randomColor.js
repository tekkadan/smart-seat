// Source: https://stackoverflow.com/questions/1484506/random-color-generator-in-javascript
var randomColor = function(){
	return "#"+((1<<24)*Math.random()|0).toString(16);
}

module.exports = randomColor;