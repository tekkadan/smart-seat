'use strict';

const Type = {
	Normal: "Normal",
	Class: "Class",
	Test: "Test"
};

var fromString = function(str){
	if(str.toLowerCase() == 'normal') return Type.Normal;
	if(str.toLowerCase() == 'class') return Type.Class;
	if(str.toLowerCase() == 'test') return Type.Test;
	throw new Error("Undefined ScenarioType");
}

module.exports = { Type, fromString };